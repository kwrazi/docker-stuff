#!/usr/bin/env bash
#
# Kiet To <kwrazi@gmail.com>
# November 2013
#

DOCKERNAME='archlinux/phpmyadmin'

function ensure_rootuser() {
	if [ $(id -u) -ne 0 ]; then
		echo "This docker install script requires root user."
		exit
	fi
}

function ensure_archlinux () {
	if [ ! -x /usr/bin/pacman ]; then
		echo "This is not an Arch Linux distro. Aborting."
		exit 1
	fi
}

function ensure_package () {
	ensure_archlinux
	for i in $*; do
		if pacman -Qq $i 2> /dev/null > /dev/null; then
			echo "Package $i installed already. Skipping."
		else
			ensure_rootuser
			echo "Installing $i..."
			pacman -S $i --noconfirm
		fi
	done
}

function generate_hard_password () {
	PASSWORD=`tr -cd '[:alnum:]' < /dev/urandom | head -c16`
}

function generate_password () {
	ensure_package pwgen
	PASSWORD=`pwgen 16 1`
	echo "----- Generating random password -----"
	echo "Password: ${PASSWORD}"
	read -p "Re-pick? [Y/n] " choice
	while [[ ! $choice =~ ^[Nn]$ ]]; do
		PASSWORD=`pwgen 16 1`
		echo "Generated password: ${PASSWORD}"
		read -p "Re-pick? [Y/n]" choice
	done
	echo "-----"
}

function create_files() {
	USERNAME=root
	generate_password
	
	echo "------ NOTE --------"
	echo "MariaDB admininstrator account details"
	echo "  username: ${USERNAME}"
	echo "  password: ${PASSWORD}"
	echo "--------------------"
	
    # create sql initialize script
	cat > mysql_setup.sql <<EOF
# Add root password
SELECT User, Host, Password FROM mysql.user;
UPDATE mysql.user SET Password = PASSWORD('${PASSWORD}') WHERE User = 'root';
FLUSH PRIVILEGES;
# Remove anonymous accounts
SELECT User, Host, Password FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE user='';
FLUSH PRIVILEGES;
# Remove test database
DROP DATABASE test;
EOF
    
    # create start.sh
    SCRIPT=start.sh
    WORKDIR=/var/lib/mysql
    if [ ! -f "${SCRIPT}" ]; then
        cat > "${SCRIPT}" <<EOF
#!/usr/bin/env bash

chown mysql:mysql ${WORKDIR}
chown mysql:mysql -R ${WORKDIR}/*

/usr/bin/mysqld_safe &
sleep 5s

if pgrep mysqld > /dev/null; then
   cd ${WORKDIR}
   mysql -u root < mysql_setup.sql

   killall mysqld
   sleep 5s
   ip addr show dev eth0
   /usr/bin/mysqld_safe
fi
EOF
    fi
    chmod 755 "${SCRIPT}"
}

function enable_ssl() {
	cd /etc/httpd/conf
	openssl genrsa -out server.key 2048
	chmod 600 server.key
	openssl req -new -key server.key -out server.csr -config openssl.cnf -batch
	openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
}

function install_phpmyadmin () {
	cp /etc/webapps/phpmyadmin/apache.example.conf /etc/httpd/conf/extra/httpd-phpmyadmin.conf
	cd /usr/share/webapps/phpMyAdmin
	mkdir config
	chgrp http config
	chmod g+w config
}

ensure_rootuser
create_files

docker build -t ${DOCKERNAME} .
