#!/usr/bin/env bash
# 
# <kwrazi@gmail.com> - November 2013
# 

DOCKER_NAME=archlinux/phpmyadmin
HOST_VOLUME=mysql
DOCK_VOLUME=/var/lib/mysql

if [[ $UID != 0 ]]; then
	echo "Please start the script as root or sudo!"
    exit 1
else
	docker run -i -t -v ${HOST_VOLUME}:${DOCK_VOLUME} ${DOCKER_NAME}
fi
