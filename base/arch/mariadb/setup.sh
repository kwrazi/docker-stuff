#!/bin/bash
#
# Kiet To <kwrazi@gmail.com>
# 2014-01-21
#

# ArchLinux mysql user/group id
MYSQL_DIR="${PWD}/mysql"
MYSQL_UID=89
MYSQL_GID=89

DOCK_IMAGE="arch/mariadb"
RUN_CMD="docker run -t -i -rm -p 3306 -v ${MYSQL_DIR}:/var/lib/mysql"

function build() {
    DOCK_IMAGE="$1"

    if docker images | grep -q "${DOCK_IMAGE}"; then
        echonote "Docker image '${DOCK_IMAGE}' already exists."
    else
        docker build -t="${DOCK_IMAGE}" -rm .
    fi
    
    if [ ! -d ${MYSQL_DIR} ]; then
        echo "Create new mysql volume for docker."
        mkdir -pv ${MYSQL_DIR}
        chown ${MYSQL_UID}:${MYSQL_GID} ${MYSQL_DIR}
    else
        echo "Docker directory ${MYSQL_DIR} already exists."
    fi
    
    # copy script into directory
    echo "Copy scripts...."
    cp -uvp mariadb/*.sh ${MYSQL_DIR}
}

function get_mysql_id () {
    DOCK_IMAGE="$1"

    if docker images | grep -q "${DOCK_IMAGE}"; then
        echonote "Docker image '${DOCK_IMAGE}' exists."
        MYSQL_UID=$(${RUN_CMD} ${DOCKER_IMAGE} id -u)
        MYSQL_GID=$(${RUN_CMD} ${DOCKER_IMAGE} id -g)
    else
        echoerr "Docker image ${DOCKER_IMAGE} does not exists."
        echoerr "Please build Dockerfile"
        exit 1
    fi
}

function run() {
    DOCK_IMAGE="$1"
    if [ ! -f ${MYSQL_DIR}/startup.sh ]; then
        build ${DOCK_IMAGE}
    fi
    ${RUN_CMD} ${DOCK_IMAGE}
}

function shell () {
    DOCK_IMAGE="$1"
    if [ ! -f "${MYSQL_DIR}/startup.sh" ]; then
        build ${DOCK_IMAGE}
    fi
    ${RUN_CMD} ${DOCK_IMAGE} /usr/bin/bash
}

function clean () {
    if [ -f "${MYSQL_DIR}/startup.sh" ]; then
        if ! diff "${MYSQL_DIR}/startup.sh" mariadb/startup.sh; then
            cp -iv "${MYSQL_DIR}/startup.sh" mariadb/startup.sh
        fi
        rm -rf mysql
    fi
}

function rmi () {
    DOCK_IMAGE="$1"
    DOCK_CONT=$(docker ps -a -q)
    if [ ! -z "${DOCK_CONT}" ]; then
        docker rm ${DOCK_CONT}
    fi
    docker rmi ${DOCK_IMAGE}
}

case "$1" in
    build)
        build ${DOCK_IMAGE}
        ;;
    run)
        run ${DOCK_IMAGE}
        ;;
    shell)
        shell ${DOCK_IMAGE}
        ;;
    rmi)
        rmi ${DOCK_IMAGE}
        ;;
    clean)
        clean
        ;;
    *)
        cat <<EOF
Syntax:
   $0 [build|run|shell|rmi|clean]
Description:
   build - build the docker file and create local volume with scripts
   run   - run the docker without argument
   shell - run the docker with shell prompt
   clean - remove the mysql volume
   rmi   - remove the docker images and containers
EOF
        ;;
esac
