#!/usr/bin/bash
# 
# Kiet To <kwrazi@gmail.com>
# 2014-01-21
#

# Assume the following packages are installed:
#   mariadb mariadb-clients expect pwgen

MYSQL_DIR=/var/lib/mysql
MYSQL_PID=${MYSQL_DIR}/mysql.pid
MYSQL_LOG=${MYSQL_DIR}/mysql.log

if [ ! -f functions.sh ]; then
    echo "functions.sh not found. Aborting."
    exit 1
fi
. functions.sh

function start_mysqld () {
    MYSQLD_CMD="/usr/bin/mysqld_safe --datadir=${MYSQL_DIR} --log-error=${MYSQL_LOG} --pid-file=${MYSQL_PID}"
    if [ -f "${MYSQL_PID}" ]; then
        if kill -0 $(cat "${MYSQL_PID}") 2> /dev/null; then
            echonote "Mariadb daemon is running."
            return
        else
            rm "${MYSQL_PID}"
        fi
    fi
    ${MYSQLD_CMD} &
    sleep 5
}

function restart_mysqld () {
    MYSQLD_CMD="/usr/bin/mysqld_safe --datadir=${MYSQL_DIR} --log-error=${MYSQL_LOG} --pid-file=${MYSQL_PID}"
    if [ -f "${MYSQL_PID}" ]; then
        echonote "Daemon found, killing old process."
        kill $(cat "${MYSQL_PID}")
        sleep 5
    fi
    ${MYSQLD_CMD} &
    sleep 5
}

function init_datadir () {
    if [ ! -f ${MYSQL_DIR}/ibdata1 ]; then
        echonote "Initializing ${MYSQL_DIR} directory."
        /usr/bin/mysql_install_db --basedir=/usr --datadir=${MYSQL_DIR}
        chmod 711 ${MYSQL_DIR}
    fi
}

function secure_mariadb () {
    # start daemon
    start_mysqld

    # check if root@localhost password is empty
    if mysqladmin -u root@localhost status 2> /dev/null > /dev/null; then
        echonote "No password found for MariadB. Assuming fresh installation."
        AUTHFILE="mariadb_auth.sh" 
        if [ -f ${AUTHFILE} ]; then
            . ${AUTHFILE}
        fi
        if [ -z "${MYSQLPASSWD}" ]; then
            echonote "No MYSQLPASSWD variable found in ${AUTHFILE}."
            echonote "Generating new password."
            MYSQLPASSWD=$(/usr/bin/pwgen 16 1)
            echo "MYSQLPASSWD=${MYSQLPASSWD}" >> ${AUTHFILE}
        fi
        echo "Securing mariadb..."
        cat <<EOF
UPDATE mysql.user SET Password=PASSWORD('$MYSQLPASSWD') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
EOF
        /usr/bin/mysql -u root <<EOF
# set root password
UPDATE mysql.user SET Password=PASSWORD('$MYSQLPASSWD') WHERE User='root';
# remove anonymous users
DELETE FROM mysql.user WHERE User='';
# remove remote root
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
# remove test database
DROP DATABASE test;
# remove privileges on test database
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
# reload privilege tables
FLUSH PRIVILEGES;
EOF
    else
        echonote "Mariadb already secured."
    fi
    
}

function secure_mariadb_old () {
    # start daemon
    start_mysqld

    # check if root@localhost password is empty
    if mysqladmin -u root@localhost status 2> /dev/null > /dev/null; then
        echonote "No password found for MariadB. Assuming fresh installation."
        AUTHFILE="mariadb_auth.sh" 
        if [ -f ${AUTHFILE} ]; then
            . ${AUTHFILE}
        fi
        if [ -z "${MYSQLPASSWD}" ]; then
            echonote "No MYSQLPASSWD variable found in ${AUTHFILE}."
            echonote "Generating new password."
            MYSQLPASSWD=$(/usr/bin/pwgen 16 1)
            echo "MYSQLPASSWD=${MYSQLPASSWD}" >> ${AUTHFILE}
        fi
        expect -c "
    set timeout 10
    spawn /usr/bin/mysql_secure_installation
    expect \"Enter current password for root\"
    send \"\r\"
    expect \"Set root password\"
    send \"y\r\"
    expect \"New password\"
    send \"${MYSQLPASSWD}\r\"
    expect \"Re-enter new password\"
    send \"${MYSQLPASSWD}\r\"
    expect \"Remove anonymous users\"
    send \"y\r\"
    expect \"Disallow root login remotely\"
    send \"n\r\"
    expect \"Remove test database and access to it\"
    send \"y\r\"
    expect \"Reload privilege tables now\"
    send \"y\r\"
    expect eof
    "
        if [ $? -ne 0 ]; then
            echoerr "Expect died unexpectedly. Aborting."
            exit 1
        fi
    fi
}

init_datadir
secure_mariadb

