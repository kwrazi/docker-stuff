#!/usr/bin/env bash
# 
# Kiet To <kwrazi@gmail.com>
# 2014-01-21
#

OS=ArchLinux

function echoerr () {
    echo -e "\e[31m$@\e[0m" 1>&2;
}

function echohi () {
    echo -e "\e[32m$@\e[0m";
}

function echonote () {
    echo -e "\e[33m$@\e[0m";
}

function update_packages() {
    # update packages
    echohi "Updating packages..."
    case $OS in
        Fedora)
            sudo yum -y update
            ;;
        ArchLinux)
            sudo pacman -Syu
            ;;
        Ubuntu)
            apt-get update
            ;;
        *)
            echoerr "Unknown OS distribution."
            exit 1
            ;;
    esac
}

function install_package() {
    PKGNAME="$@"
    case $OS in
        Fedora)
            if ! rpm -q --quiet ${PKGNAME}; then
                echohi "Installing ${PKGNAME}..."
                sudo yum -y install ${PKGNAME}
            else
                echonote "Package ${PKGNAME} already installed. Skipping."
            fi
            ;;
        ArchLinux)
            if ! pacman -Qq ${PKGNAME} 2> /dev/null; then
                echohi "Installing ${PKGNAME}..."
                sudo pacman -Sy ${PKGNAME} --noconfirm
            else
                echonote "Package ${PKGNAME} already installed. Skipping."
            fi
            ;;
        Ubuntu)
            echoerr "Fix me."
            exit 1
            ;;
        *)
            echoerr "Unknown OS distribution."
            exit 1
            ;;
    esac
}

