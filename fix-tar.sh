#!/bin/bash
#
# Kiet To - November 2013
# Create older GNU tar 1.26 to downgrade tar 1.27 for archlinux docker
# This is to fix issue describe in
#     https://github.com/dotcloud/docker/issues/2403
#     https://github.com/dotcloud/docker/issues/2643
#
# Run this script on an Arch Linux distro
#

# unprivileged (non-root) user
USER=docker

INSTALLDIR="tar-1.26"

if pacman -Q tar | grep -q '1.26'; then
    echo "Workng version (v1.26) already installed. Skipping."
	exit
fi

if [ ! -f tar-1.26-*.pkg.tar.xz ]; then
	if [ ! -d "${INSTALLDIR}" ]; then
		echo "Making install directory ${INSTALLDIR}..."
		mkdir -p "${INSTALLDIR}"
	fi
	
	cd "${INSTALLDIR}"
	
	if [ ! -f tar-1.26-*.pkg.tar.xz ]; then
		# Get the relevent files
		curl -s "https://projects.archlinux.org/svntogit/packages.git/plain/trunk/PKGBUILD?h=packages/tar" | \
			sed -e 's/pkgver=.*$/pkgver=1.26/' | \
			sed -e 's/[0-9a-f]\{32\}/0ced6f20b9fa1bea588005b5ad4b52c1/' | \
			sed -e '/configure/a patch -p1 < ../../tar-1.26-no-gets.patch'> PKGBUILD
		curl -s https://projects.archlinux.org/svntogit/packages.git/plain/trunk/tar.install?h=packages/tar > tar.install
		curl -s http://git.buildroot.net/buildroot/plain/package/tar/tar-1.26-no-gets.patch > tar-1.26-no-gets.patch

		if [ "$(id -u)" != "0" ]; then
			makepkg -s -c --noconfirm
		else
			runuser -l ${USER} -c 'makepkg -s -c --noconfirm'
		fi
	else
		echo "tar-1.26 package found. Skipping build."
	fi

	mv tar-1.26-*.pkg.tar.xz ..
	cd ..
fi

echo "Install tar-1.26 into host archlinux machine."
