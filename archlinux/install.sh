#!/usr/bin/env bash
#
# Kiet To <kwrazi@gmail.com>
# November 2013
#

DOCKERNAME='archlinux'

function ensure_rootuser() {
	if [ $(id -u) -ne 0 ]; then
		echo "This docker install script requires root user."
		exit
	fi
}

ensure_rootuser

docker build -t ${DOCKERNAME} .
