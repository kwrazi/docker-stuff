#!/usr/bin/env bash
#
# Kiet To <kwrazi@gmail.com>
# November 2013
#

DOCKERNAME='archlinux/mariadb'

function ensure_rootuser() {
	if [ $(id -u) -ne 0 ]; then
		echo "This docker install script requires root user."
		exit
	fi
}

function create_files() {
	USERNAME=root
	PASSWORD=`tr -cd '[:alnum:]' < /dev/urandom | head -c8`
	
	echo "------ NOTE --------"
	echo "MariaDB admininstrator account details"
	echo "  username: ${USERNAME}"
	echo "  password: ${PASSWORD}"
	echo "--------------------"
	
    # create sql initialize script
	cat > mysql_setup.sql <<EOF
# Add root password
SELECT User, Host, Password FROM mysql.user;
UPDATE mysql.user SET Password = PASSWORD('${PASSWORD}') WHERE User = 'root';
FLUSH PRIVILEGES;
# Remove anonymous accounts
SELECT User, Host, Password FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE user='';
FLUSH PRIVILEGES;
# Remove test database
DROP DATABASE test;
EOF
    
    # create start.sh
    SCRIPT=start.sh
    WORKDIR=/var/lib/mysql
    if [ ! -f "${SCRIPT}" ]; then
        cat > "${SCRIPT}" <<EOF
#!/usr/bin/env bash

chown mysql:mysql ${WORKDIR}
chown mysql:mysql -R ${WORKDIR}/*

/usr/bin/mysqld_safe &
sleep 5s

if pgrep mysqld > /dev/null; then
   cd ${WORKDIR}
   mysql -u root < mysql_setup.sql

   killall mysqld
   sleep 5s
   ip addr show dev eth0
   /usr/bin/mysqld_safe
fi
EOF
    fi
    chmod 755 "${SCRIPT}"
}

ensure_rootuser
create_files

docker build -t ${DOCKERNAME} .
