#!/bin/bash
# Generate a minimal filesystem for archlinux and load it into the local
# docker as "archlinux"
# requires root
#
# modified by Kiet To - November 2013
#

#################################################################
# docker image name
DOCKNAME=archlinux
# packages to install during bootstrap
PACKAGES="base base-devel rsync yajl haveged"
# packages to ignore for space savings
PKGIGNORE=linux,jfsutils,lvm2,cryptsetup,groff,man-db,man-pages,mdadm,pciutils,pcmciautils,reiserfsprogs,s-nail,xfsprogs
# set timezone
TZ=Australia/Adelaide
#################################################################

function check_executable() {
    if [ -z "$1" ]; then return; else EXE="$1"; fi
    if [ -z "$2" ]; then
        MSG="Could not find executable ${EXE}."
    else
        MSG="$2"
    fi
    if [ -z "`which ${EXE} 2> /dev/null`" ]; then
        echo "${MSG}"
        exit 1
    fi
}

function check_archlinux() {
    check_executable pacman \
        "Could not find pacman. Does not appear to be an ArchLinux distro"
    if [ "$(id -u)" != "0" ]; then
        echo "This script should be executed as root"
        exit 1
    fi
}

check_archlinux
check_executable pacstrap \
    "Could not find pacstrap. Run pacman -S arch-install-scripts"
check_executable expect \
    "Could not find expect. Run pacman -S expect"

set -e

ROOTFS=~/rootfs-arch-$$-$RANDOM
mkdir $ROOTFS

expect <<EOF
  set timeout 60
  set send_slow {1 1}
  spawn pacstrap -c -d -G -i $ROOTFS $PACKAGES --ignore $PKGIGNORE
  expect {
    "Install anyway?" { send n\r; exp_continue }
    "(default=all)" { send \r; exp_continue }
    "Proceed with installation?" { send "\r"; exp_continue }
    "skip the above package" {send "y\r"; exp_continue }
    "checking" { exp_continue }
    "loading" { exp_continue }
    "installing" { exp_continue }
  }
EOF

arch-chroot $ROOTFS /bin/sh -c "haveged -w 1024; pacman-key --init; pkill haveged; pacman -Rs --noconfirm haveged; pacman-key --populate archlinux"
arch-chroot $ROOTFS /bin/sh -c "ln -s /usr/share/zoneinfo/${TZ} /etc/localtime"
cat > $ROOTFS/etc/locale.gen <<DELIM
en_US.UTF-8 UTF-8
en_US ISO-8859-1
DELIM
arch-chroot $ROOTFS locale-gen
arch-chroot $ROOTFS /bin/sh -c 'echo "Server = http://mirrors.kernel.org/archlinux/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist'

# udev doesn't work in containers, rebuild /dev
DEV=${ROOTFS}/dev
mv ${DEV} ${DEV}.old
mkdir -p ${DEV}
mknod -m 666 ${DEV}/null c 1 3
mknod -m 666 ${DEV}/zero c 1 5
mknod -m 666 ${DEV}/random c 1 8
mknod -m 666 ${DEV}/urandom c 1 9
mkdir -m 755 ${DEV}/pts
mkdir -m 1777 ${DEV}/shm
mknod -m 666 ${DEV}/tty c 5 0
mknod -m 600 ${DEV}/console c 5 1
mknod -m 666 ${DEV}/tty0 c 4 0
mknod -m 666 ${DEV}/full c 1 7
mknod -m 600 ${DEV}/initctl p
mknod -m 666 ${DEV}/ptmx c 5 2

USER=docker
USERHOME=/home/${USER}

function aur_install_init() {
	# create makepkg user so that we don't have to use --asroot parameter
	arch-chroot $ROOTFS /bin/useradd -m ${USER}
	
	INSTALLDIR="${ROOTFS}${USERHOME}/install"
	if [ ! -d "${INSTALLDIR}" ]; then
		echo "Creating AUR install directory..."
		arch-chroot ${ROOTFS} /bin/runuser -l ${USER} -c "mkdir ${USERHOME}/install"
	fi
}

function aur_install() {
	PKG_URL="$1"
	INS_URL="$2"
	INSTALLDIR="${ROOTFS}${USERHOME}/install"
	if [ ! -z "${PKG_URL}" ]; then
		# install package-query from AUR for yaourt
		wget -c "${PKG_URL}"
		/bin/mv -v PKGBUILD "${INSTALLDIR}"
	else
		echo "No PKGBUILD"
		return 1
	fi
	if [ ! -z "${INS_URL}" ]; then
		wget -c "${INS_URL}"
		/bin/mv -v *.install "${INSTALLDIR}"
	fi
	arch-chroot ${ROOTFS} /bin/sh -c "runuser -l ${USER} -c 'cd install; makepkg -s -c --noconfirm'"
	arch-chroot ${ROOTFS} /bin/sh -c "pacman --noconfirm -U ${USERHOME}/install/*.xz"
	/bin/rm ${INSTALLDIR}/*.xz
}

function aur_install_end() {
	INSTALLDIR="${ROOTFS}/root/install"
	if [ -d "${INSTALLDIR}" ]; then
		echo "Removing AUR install directory..."
		rm -rf "${INSTALLDIR}"
	fi
}

aur_install_init
###########################
# install package-query from AUR (yaourt dependency)
aur_install "https://aur.archlinux.org/packages/pa/package-query/PKGBUILD"
# install yaourt from AUR
aur_install "https://aur.archlinux.org/packages/ya/yaourt/PKGBUILD"
###########################
# temporary fix for tar 1.27 until upstream fixes
./fix-tar.sh
###########################
aur_install_end

# clean packages and database to free image space
arch-chroot $ROOTFS /bin/sh -c 'yaourt --noconfirm -Scc'
# set locale.conf
cat > ${ROOTFS}/etc/locale.conf <<EOF
LANG="en_US.UTF-8"
LC_ALL="en_US.UTF-8"
LC_COLLATE="C"
EOF
cat >> ${ROOTFS}/etc/bash.bashrc <<EOF
# Hack to fix missing tty in bash shell. For detail, see:
#   https://github.com/dotcloud/docker/issues/728
[ ! -c "`tty`" ] && exec > /dev/tty 2> /dev/tty < /dev/tty
EOF

tar --numeric-owner -C $ROOTFS -c . | docker import - "${DOCKNAME}"
docker run -i -t "${DOCKNAME}" echo Success.
rm -rf $ROOTFS
